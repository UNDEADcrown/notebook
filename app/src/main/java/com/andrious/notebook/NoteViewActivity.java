package com.andrious.notebook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by MD.ISRAFIL MAHMUD on 7/15/2017.
 */

public class NoteViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_view);

        final EditText titleview = (EditText) findViewById(R.id.title_view);
        final EditText textview = (EditText) findViewById(R.id.text_view);
        final EditText dateview = (EditText) findViewById(R.id.date_view);
        final EditText timeview = (EditText) findViewById(R.id.time_view);
        Button update_button = (Button) findViewById(R.id.update_button);

        final String sub_id = getIntent().getStringExtra("id");
        final String title = getIntent().getStringExtra("title");
        final String text = getIntent().getStringExtra("text");
        final String date = getIntent().getStringExtra("date");
        final String time = getIntent().getStringExtra("time");
        titleview.setText(title);
        textview.setText(text);
        dateview.setText(date);
        timeview.setText(time);


        update_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExampleDBHelper exampleDBHelper = new ExampleDBHelper(getApplicationContext());

                String updateTitle = titleview.getText().toString();
                String updateText = textview.getText().toString();
                String updateDate = dateview.getText().toString();
                String updateTime = timeview.getText().toString();

                Intent i = new Intent(NoteViewActivity.this, MainActivity.class);
                exampleDBHelper.updatePerson(sub_id, updateTitle, updateText, updateDate, updateTime);


                startActivity(i);


            }
        });
    }

}